<?php

namespace App\Controller;

use App\Entity\Paris;
use App\Repository\ParisRepository;
use AppBundle\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @Route("/api")
 */
class ParisController extends Controller
{
    /**
     * @Route("/paris", name="paris", methods="GET")
     * @return Response
     */
    public function getParis(ParisRepository $repository)
    {
        $paris = $repository->findAll();

        foreach($paris as $details)
        {
            $formattedParis[] =
                [
                    'id' => $details->getId(),
                    'district' => $details->getDistrict(),
                    'borough' => $details->getBorough(),
                    'nb_hotels' => $details->getCountHotel(),
                    'googleMaps' => [
                        'type' => 'point',
                        'longitude' => $details->getLongitude(),
                        'latitude' =>$details->getLatitude()
                    ]
                ];
        }

        $formattedParis = $this->get('jms_serializer')->serialize($formattedParis, 'json');

        $response = new Response($formattedParis);

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }


    /**
     * @Route("/paris/{id}", name="api_paris_show", methods={"GET","HEAD"})
     * @param Paris $paris
     * @return Response
     */
    public function showParis(Paris $paris)
    {
        $paris = [
            'id' => $paris->getId(),
            'district' => $paris->getDistrict(),
            'borough' => $paris->getBorough(),
            'nb_hotels' => $paris->getCountHotel(),
            'googleMaps' => [
                'type' => 'point',
                'longitude' => $paris->getLongitude(),
                'latitude' => $paris->getLatitude()
            ]

        ];

        $paris = $this->get('jms_serializer')->serialize($paris, 'json');

        $response = new Response($paris);

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/paris/{id}/edit", name="api_paris_edit", methods={"PUT","PATCH"})
     * @param Paris $paris
     * @return Response
     */
    public function editParis(Paris $paris, Request $request)
    {
        $data = $request->getContent();
        $content = $this->get('jms_serializer')->deserialize($data, 'App\Entity\Paris', 'json');
        dd($content);
        $paris = $this->get('jms_serializer')->deserialize($data, $paris, 'json');
        $em = $this->getDoctrine()->getManager();
        $em->persist($paris);
        $em->flush();



        $response = new Response($paris);

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * @Route("/paris/create", name="api_paris_create", methods="POST")
     * @param Request $request
     * @return Response
     */
    public function createParis(Request $request)
    {
        $data = $request->getContent();
        $paris = $this->get('serializer')->deserialize($data, 'App\Entity\Paris', 'json');
        $em = $this->getDoctrine()->getManager();
        $em->persist($paris);
        $em->flush();

        return new Response('created', Response::HTTP_CREATED);
    }
}