<?php

namespace App\Controller;

use App\Entity\Paris;
use App\Form\ParisType;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpClient\HttpClient;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/admin")
 */
class DashboardController extends AbstractController
{

    /**
     * @var ObjectManager
     */
    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @Route("/home", name="admin_home")
     */
    public function home()
    {
        return $this->render('dashboard/index.html.twig');
    }

    /**
     * @Route("/paris", name="admin_index_paris")
     */
    public function indexParis()
    {
        $client = HttpClient::create();

        $response = $client->request('GET', 'http://localhost:8000/api/paris');

        $paris = $response->toArray();

        return $this->render('dashboard/paris/index.html.twig', compact('paris'));
    }

    /**
     * @Route("/paris/create", name="admin_create_paris")
     */
    public function createParis(Request $request)
    {
        $paris = new Paris();

        $form = $this->createForm(ParisType::class, $paris);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $client = HttpClient::create();

            $client->request('POST', 'http://localhost:8000/api/paris/create', [
                'json' => [
                    'borough' => $form->get('district')->getData(),
                    'district' => $form->get('borough')->getData(),
                    'count_hotel' => $form->get('count_hotel')->getData(),
                    'latitude' => $form->get('latitude')->getData(),
                    'longitude' => $form->get('longitude')->getData()
                ]
            ]);

            $this->addFlash('success', 'Article Created! Knowledge');
            return $this->redirectToRoute('admin_index_paris');
        }
        return $this->render('dashboard/paris/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/paris/{id}", name="admin_show_paris")
     */
    public function showParis($id)
    {
        $client = HttpClient::create();

        $response = $client->request('GET', 'http://localhost:8000/api/paris/'.$id);

        $paris = $response->toArray();

        return $this->render('dashboard/paris/show.html.twig', compact('paris'));
    }

    /**
     * @Route("/paris/{id}/edit", name="admin_update_paris")
     */
    public function editParis(Paris $paris, Request $request)
    {
        $form = $this->createForm(ParisType::class, $paris);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid())
        {
            $this->manager->flush();
            return $this->redirectToRoute('admin_home');
        }
        return $this->render('dashboard/paris/edit.html.twig',[
            'paris' => $paris,
            'form' => $form->createView()
        ]);
    }

}