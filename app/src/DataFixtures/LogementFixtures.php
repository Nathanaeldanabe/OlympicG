<?php
namespace App\DataFixtures;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Logement;


class LogementFixtures extends Fixture
{
    public function load(ObjectManager $em)
    {
      {
        $logement = new Logement();   
        $logement->setLocationPrice('Paris ');
        $logement->setType('Paris ');
        $em->persist($logement);
      }
       $em->flush();
    }           
}