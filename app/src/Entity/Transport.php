<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TransportRepository")
 */
class Transport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $number_rer;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $number_metro;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $number_bus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumberRer(): ?string
    {
        return $this->number_rer;
    }

    public function setNumberRer(?string $number_rer): self
    {
        $this->number_rer = $number_rer;

        return $this;
    }

    public function getNumberMetro(): ?string
    {
        return $this->number_metro;
    }

    public function setNumberMetro(?string $number_metro): self
    {
        $this->number_metro = $number_metro;

        return $this;
    }

    public function getNumberBus(): ?string
    {
        return $this->number_bus;
    }

    public function setNumberBus(?string $number_bus): self
    {
        $this->number_bus = $number_bus;

        return $this;
    }
}
