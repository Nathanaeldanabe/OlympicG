import React, {Component} from 'react';
import axios from 'axios';
    
class Paris extends Component {

    constructor() {
        super();
        this.state = { paris: [], loading: true };
    }
    
    componentDidMount() {
        console.log(this.getParis());
    }
    
    getParis() {
       axios.get(`http://localhost:8000/api/paris`).then(paris => {
           this.setState({ paris: paris.data, loading: false})
       })
    }
    
    render() {
        const loading = this.state.loading;
        return(
            <div>
                <section className="row-section">
                    <div className="container">
                        <div className="row">
                        </div>
                        {loading ? (
                            <div className={'row text-center'}>
                                <span className="fa fa-spin fa-spinner fa-4x"></span>
                            </div>
                        ) : (
                            <div className={'row'}>
                                { this.state.paris.map(details =>
                                    <div className="col-md-10 offset-md-1 row-block" key={details.id}>
                                        <p>{ details.borough }</p>
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                </section>
            </div>
        )
    }
}
export default Paris;